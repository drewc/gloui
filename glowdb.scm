(define %glowdb #f)

(##inline-host-declaration
 #<<EOF
 const { openDB, deleteDB, wrap, unwrap } = require('idb');
 (() => {
     globalThis.$glowdb = false;
     globalThis.$glowdbPromise = false;
     const dbName = 'glow'
     const version = 25 //versions start at 1

     const entities = ['address'];

     console.log('Loading DB')

  const db = openDB(dbName, version, {
    upgrade(db, oldVersion, newVersion, transaction) {
      console.log('Upgrading DB')
      entities.map(e => {
        console.log('Entity', e)
       if (!db.objectStoreNames.contains(e)) {
          db.createObjectStore(e)
       }
      })
     // db.deleteObjectStore('network')
      console.log('Upgrading Network')
     if (!db.objectStoreNames.contains('network')) {
       let objectStore = db.createObjectStore("network", { keyPath: "key" });
        objectStore.createIndex("description", "description", { unique: true });
        objectStore.createIndex("uri", "uri", { unique: true });
     }
     

     // const crypt = db.deleteObjectStore('crypto')
     // globalThis.$c = crypt;

      console.log('Upgrading Crytpo')
      if (!db.objectStoreNames.contains('crypto')) {
        let objectStore = db.createObjectStore("crypto", { keyPath: "symbol" });
      }
      

      console.log('Upgrading Asset')
      if (!db.objectStoreNames.contains('asset')) {
        let objectStore = db.createObjectStore("asset", { keyPath: "id" });
      }
      

      console.log('Upgrading Resource Type')
      if (!db.objectStoreNames.contains('resourceType')) {
        let objectStore = db.createObjectStore("resourceType", { keyPath: "name" });
      }
      

      // db.deleteObjectStore('resource')
      console.log('Upgrading Resource')
      if (!db.objectStoreNames.contains('resource')) {
        let objectStore = db.createObjectStore("resource", { keyPath: "id" });
      }
      

      //db.deleteObjectStore('contact')
      console.log('Upgrading contact')
     if (!db.objectStoreNames.contains('contact')) {
       let objectStore = db.createObjectStore("contact", { keyPath: "id" });
     }

    }
  })

  $glowdbPromise = db;

  db.then(d => { globalThis.$glowdb = d })
})()


EOF
)


(define (glowdb)
  (or %glowdb
      (let foo ()
        (let ((db (##inline-host-expression "@host2scm@($glowdb)")))
          (if db
            (begin (set! %glowdb db) db)
            (begin (thread-sleep! 0.01)
                   (foo)))))))

(##begin
  (##namespace ("_six/js#" six.infix))
  (##define-syntax six.infix
    (lambda (src)
      (##demand-module _six/six-expand)
      (_six/six-expand#six.infix-js-expand src))))

(define (store name)
  \`(glowdb).transaction(`name).objectStore(`name))

(define (store.add name object #!optional (key \undefined))
  \`(glowdb).transaction(`name, 'readwrite').objectStore(`name).add(`object,`key))

(define (store.put name object #!optional (key #!void))
  \`(glowdb).transaction(`name, 'readwrite').objectStore(`name).put(`object, `key))

(define (store.get name key #!optional (value #!void))
  (define store \`(glowdb).transaction(`name).objectStore(`name))
   (if (void? value)
     \(`store).get(`key)
     (begin
       \console.log((`store).indexNames)
      (let ((idx \(`store).index(`(keyword->string key))))
      \(`idx).get(`value)))))



(define (ensureNetwork nw)
  (if (list? nw)
    (##inline-host-expression "@host2scm@({ key: @scm2host@(@1@), description: @scm2host@(@2@), uri: @scm2host@(@3@),
   nativeCurrency: @scm2host@(@4@), test: @scm2host@(@5@)})"
                        (car nw) (cadr nw) (caddr nw) (cadddr nw) (cadddr (cdr nw)))
    nw))

(define (%network-add/put add/put nw)
  (define nwjs (ensureNetwork nw))
  (##inline-host-statement "console.log(@1@)" nwjs)
  (add/put "network" nwjs))

(define (network.add nw)
  (%network-add/put store.add nw))
(define (network.put nw)
  (%network-add/put store.put nw))

(define (network.get key #!optional (value #!void))
  (store.get "network" key value))

(define (add-networks)
  (let ((networks
       '(("ced"         "Cardano EVM Devnet"  "https://rpc-evm.portal.dev.cardano.org/" "ADA" #f)
        ("etc"         "Ethereum Classic Mainnet"            "https://ethereumclassic.network" "ETC" #f)
        ("eth"         "Ethereum Mainnet"                    "https://mainnet.infura.io/v3/${INFURA_API_KEY}" "ETH" #f)
        ("kot"         "Ethereum Classic Testnet Kotti"      "https://www.ethercluster.com/kotti" "ETC" #t)
        ("kov"         "Ethereum Testnet Kovan"             "https://kovan.poa.network" "ETH" #t)
        ("ogor"        "Optimistic Ethereum Testnet Goerli" "https://www.ethercluster.com/goerli" "ETH" #t)
        ("pet"         "Private Ethereum Testnet"
         "http://localhost:8545" "ETH" #t)
        ("rin"         "Ethereum Testnet Rinkeby"           "https://rinkeby.infura.io/v3/${INFURA_API_KEY}" "ETH" #t)
        ("rop"         "Ethereum Testnet Ropsten"           "https://ropsten.infura.io/v3/${INFURA_API_KEY}" "ETH" #t))))

  (map (lambda (nw) (let ((n (network.get (car nw))))
                      (if (void? n) (network.add nw)
                          (network.put nw))))
       networks)))
(define (list-networks)
  \`(store "network").getAll())



(##inline-host-declaration #<<EOF

function addResourceType(name, description) {
  $glowdbPromise.then(db => {
    const trans = db.transaction('resourceType', 'readwrite').objectStore('resourceType')
    const obj = trans.put({ name, description })
  })
}

addResourceType("crypto", { Symbol: "string" })
addResourceType("ERC20", { Token: "hash" })

function listResourceTypes() {
  return $glowdbPromise.then(db => {
    const trans = db.transaction('resourceType').objectStore('resourceType')
    return trans.getAll();
  })
}


function saveContact(ctct) {
  $glowdbPromise.then(db => {
    const trans = db.transaction('contact', 'readwrite').objectStore('contact')
    const obj = trans.put(ctct)
  })
}
function listContacts() {
  return $glowdbPromise.then(db => {
    const trans = db.transaction('contact').objectStore('contact')
    return trans.getAll();
  })
}

// async function ensureContactEntity(ct) {
//   const contact (typeof ct === 'string') ? await findContact(ct) : ct;
//   contact.owner = (typeof contact.)

// }
function findContact(id) {
  if (typeof id === 'object') return id;
  return $glowdbPromise.then(db => {
    const trans = db.transaction('contact').objectStore('contact')
    return trans.get(id);
  })
}
function saveResource(ctct) {
  $glowdbPromise.then(db => {
    const trans = db.transaction('resource', 'readwrite').objectStore('resource')
    const obj = trans.put(ctct)
  })
}
function listResources() {
  return $glowdbPromise.then(db => {
    const trans = db.transaction('resource').objectStore('resource')
    return trans.getAll();
  })
}
function findResource(id) {
  if (typeof id === 'object') return id;
  return $glowdbPromise.then(db => {
    const trans = db.transaction('resource').objectStore('resource')
    return trans.get(id);
  })
}
function saveAsset(ctct) {
  $glowdbPromise.then(db => {
    const trans = db.transaction('asset', 'readwrite').objectStore('asset')
    const obj = trans.put(ctct)
  })
}

function listAssets() {
  return $glowdbPromise.then(db => {
    const trans = db.transaction('asset').objectStore('asset')
    return trans.getAll();
  })
}


function findAsset(id) {
  if (typeof id === 'object') return id;
  return $glowdbPromise.then(db => {
    const trans = db.transaction('asset').objectStore('asset')
    return trans.get(id);
  })
}
async function findAssetEntity(as) {

  async function givr (give, thing) {
    const gThing = (typeof thing === 'string') ? await give(thing) : thing;
    return gThing;
  }

  const ass = await givr(findAsset, as);
  ass.resource = await givr(findResource, ass.resource)
  ass.owner = await givr(findContact, ass.owner)

  return ass;

}

async function listAssetEntities() {
  const newEns = [];
  const ens = await listAssets();

  for (const i in ens) {
    const en = await findAssetEntity(ens[i])
    newEns.push(en)
  }

  return newEns;
}

EOF
)

(##inline-host-statement "globalThis.addNetworks = @scm2host@(@1@); alert('here!')" add-networks)

(##inline-host-declaration  "module.exports = {
  listNetworks: () => {
   return $glowdb.transaction('network').objectStore('network').getAll();
  },
  addResourceType: addResourceType,
  listResourceTypes: listResourceTypes,
  saveContact: saveContact,
  findContact: findContact,
  listContacts: listContacts,
  saveResource: saveResource,
  findResource: findResource,
  listResources: listResources,
  saveAsset: saveAsset,
  listAssets: listAssets,
  listAssetEntities: listAssetEntities,

}")
(##inline-host-declaration "
globalThis.randomUUID = function (){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}
"
)
